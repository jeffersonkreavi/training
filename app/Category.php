<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    public $timestamps = false;

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
        $slug = $this->makeSlugFromName($value);
        $this->attributes['slug'] = $slug;
    }

    private function makeSlugFromName($name)
    {
        $slug = Str::slug($name);

        $count = Category::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

        return $count ? "{$slug}-{$count}" : $slug;
    }

    public function articles()
    {
//        return $this->hasMany('App\Article');
        return Article::where('category_id', $this->attributes['id'])->paginate(10);
    }
    public function scopeSorted($query)
    {
        return $query->orderBy('name','ASC');
    }
}
