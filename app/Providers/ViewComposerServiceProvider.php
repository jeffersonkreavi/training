<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigation();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Compose the navigation bar.
     */
    private function composeNavigation()
    {
        view()->composer('layout.header', 'App\Http\Composers\NavigationComposer@compose');
//        view()->composer('layout.header', function ($view)
//        {
//            $view->with('latest', Article::latest()->first());
//        });
    }

}
