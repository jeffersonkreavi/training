<?php namespace App;
// AuthenticateUser.php
use App\Listeners\AuthenticateUserListener;

use App\Repository\UserRepository;
use Illuminate\Contracts\Auth\Guard;
use Laravel\Socialite\Contracts\Factory as Socialite;
use App\User;
use Request;

/**
 * Class AuthenticateUser
 * @package App
 */
class AuthenticateUser {

    /**
     * @var Socialite
     */
    private $socialite;
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @var UserRepository
     */
    private $users;


    /**
     * @param Socialite $socialite
     * @param Guard $auth
     * @param UserRepository $users
     */
    public function __construct(Socialite $socialite, Guard $auth, UserRepository $users) {
        $this->socialite = $socialite;
        $this->users = $users;
        $this->auth = $auth;
    }

    /**
     * @param $hasCode
     * @param $provider
     * @param AuthenticateUserListener $listener
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function execute($hasCode, $provider, AuthenticateUserListener $listener) {
        if (!$hasCode) return $this->getAuthorizationFirst($provider);
        $user = $this->users->findByEmailOrCreate($this->getSocialUser($provider), $provider);
        $this->auth->login($user, true);

        return $listener->userHasLoggedIn($user);
    }

    /**
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function getAuthorizationFirst($provider) {
        return $this->socialite->driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Laravel\Socialite\Contracts\User
     */
    private function getSocialUser($provider) {
        return $this->socialite->driver($provider)->user();
    }
}