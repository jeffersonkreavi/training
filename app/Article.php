<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'short_description', 'content', 'image', 'thumbnail'
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucfirst($value);
        $slug = $this->makeSlugFromTitle($value);
        $this->attributes['slug'] = $slug;
    }

    private function makeSlugFromTitle($name)
    {
        $slug = Str::slug($name);

        $count = Article::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

        return $count ? "{$slug}-{$count}" : $slug;
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment')->latest();
    }
}
