<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordRequest extends Request {

    public function __construct() {
        $this->validator = app('validator');

        $this->validateOldPassword($this->validator);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|passcheck',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
    }
    public function messages() {
        return [
            'old_password.passcheck' => 'Your old password didn\'t match.'
        ];
    }

    /**
     * Check Old Password
     * @param $validator
     */
    public function validateOldPassword($validator) {
        $validator->extend('passcheck', function($attribute, $value, $parameters) {
            $oldPassword = \Auth::user()->getAuthPassword();
            if(Hash::check($value, $oldPassword)){
                return true;
            }else{
                return false;
            }
        });
    }


}
