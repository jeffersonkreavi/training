<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ArticlesController@index');

//Model Binding
//Route::model('category', 'App\Category');


//Route::bind('category', function($slug)
//{
//    return App\Category::where('slug', $slug)->first();
//});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('login/{provider?}', 'Auth\AuthController@login');
Route::get('user/changepassword', 'UsersController@changePassword');
Route::post('user/changepassword', 'UsersController@postChangePassword');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('articles', 'ArticlesController@index');
Route::get('articles/admin', 'ArticlesController@admin');
Route::get('articles/create', 'ArticlesController@create');
Route::get('articles/{articles_slug}', 'ArticlesController@show');
Route::post('articles', 'ArticlesController@store');
Route::get('articles/{id}/edit', 'ArticlesController@edit');
Route::patch('articles/{id}', 'ArticlesController@update');
Route::delete('articles/{id}', 'ArticlesController@destroy');

Route::post('comment', 'CommentsController@store');
Route::delete('comment/{id?}', 'CommentsController@destroy');
Route::resource('category', 'CategoriesController');