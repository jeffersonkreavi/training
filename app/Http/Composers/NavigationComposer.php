<?php namespace App\Http\Composers;


use Auth;

// Class untuk share data Auth user ke semua page
class NavigationComposer {

    /**
     * @param $view
     */
    public function compose($view){
        $view->with('auth', Auth::user());
    }
}