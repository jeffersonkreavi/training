<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function changePassword()
    {
        return view('auth.changepassword');
    }

    public function postChangePassword(Requests\ChangePasswordRequest $request)
    {
        $user = \Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect('/')->with([
            'flash_message' => 'Your password has been changed!',
            'flash_message_important' => true
        ]);
    }
}
