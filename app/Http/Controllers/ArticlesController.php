<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Comment;
use App\Resize;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{

    /**
     * Create a new article controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $articles = Article::latest('created_at')->paginate(2);
        return view('articles.index', compact('articles'));
    }

    public function admin()
    {
        $articles = Auth::user()->articles()->latest('created_at')->paginate(2);
        return view('articles.admin', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::sorted()->lists('name', 'id');
        return view('articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ArticleRequest $request
     */
    public function store(Requests\ArticleRequest $request)
    {
        $article = new Article();

        $randomString = str_random(16);
        $article->title = $request->title;
        $article->short_description = $request->short_description;
        $article->content = $request->content;
        $article->image = $randomString;
        $article->imgtype = $request->file('image')->getClientOriginalExtension();
        $article->thumbnail = $randomString.".jpg";
        $article->category_id = $request->category_list;
        $article->user_id = Auth::user()->id;
        $article->save();

        $request->file('image')->move(
            public_path() . '/images/original/', $article->image.".".$article->imgtype
        );

        $this->generateThumbnail($article->image, $article->imgtype);

        return redirect('/articles')->with([
            'flash_message' => 'Your article has been created!',
            'flash_message_important' => true
        ]);
    }

    private function generateThumbnail($filename, $type)
    {
        $imgThumbnail = new Resize();
        $size = getimagesize(public_path()."/images/original/".$filename.".".$type);
        $imgThumbnail->load(public_path()."/images/original/".$filename.".".$type);
        if($size[0]<=$size[1])
            $imgThumbnail->resizeToWidth(220);
        else
            $imgThumbnail->resizeToHeight(220);
        imagejpeg($imgThumbnail->image,public_path()."/images/thumbnail/".$filename.".jpg", 80);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
        $user = Auth::user();
        $article = Article::where('slug', $slug)->firstOrFail();
        $js = array('article.js');
        return view('articles.show', compact(['article', 'user', 'js']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);
        if(!$this->isAuthorized($article->user_id)){
            return redirect('/articles')->with([
                'flash_message' => 'Unauthorized!',
                'flash_message_important' => true
            ]);
        }
        $categories = Category::sorted()->lists('name', 'id');
        return view('articles.edit', compact('article', 'categories'));
    }

    /**
     * @param $user_id
     * @return bool
     */
    private function isAuthorized($user_id)
    {
        return (Auth::user()->id == $user_id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $article = Article::findOrFail($id);
        $article->title = Input::get('title');
        $article->short_description = Input::get('short_description');
        $article->content = Input::get('content');
        $article->category_id = Input::get('category_list');
        if(!$this->isAuthorized($article->user_id)){
            return redirect('/articles')->with([
                'flash_message' => 'Unauthorized!',
                'flash_message_important' => true
            ]);
        }
        if(Input::hasFile('image')){
            unlink(public_path()."/images/original/$article->image.$article->imgtype");
            $article->imgtype = Input::file('image')->getClientOriginalExtension();
            Input::file('image')->move(
                public_path() . '/images/original/', $article->image.".".$article->imgtype
            );
            $this->generateThumbnail($article->image, $article->imgtype);
        }
        $article->save();
        return redirect('/articles')->with([
            'flash_message' => 'Your article has been updated!',
            'flash_message_important' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        if(!$this->isAuthorized($article->user_id)){
            return redirect('/articles')->with([
                'flash_message' => 'Unauthorized!',
                'flash_message_important' => true
            ]);
        }
        unlink(public_path()."/images/original/$article->image.$article->imgtype");
        unlink(public_path()."/images/thumbnail/$article->thumbnail");
        $article->delete();
        return redirect('/articles')->with([
            'flash_message' => 'Your article has been deleted successfully!',
            'flash_message_important' => true
        ]);
    }
}
