<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\Input;

class CategoriesController extends Controller
{

    /**
     * Create a new category controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        $categories = Category::all();
        $categories = Category::sorted()->paginate(10);
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('category.create');
    }


    /**
     * @param Requests\CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Requests\CategoryRequest $request)
    {
        $category = Category::create($request->all());

        session()->flash('flash_message', 'Your category has been created!');
        return redirect('/category')->with([
            'flash_message' => "Category $request->name has been created!",
            'flash_message_important' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $articles = $category->articles();
//        dd(DB::table('categories')->articles->toSql());
        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category.edit', compact('category'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $category = Category::findOrFail($id);
        $category->name = Input::get('name');
        $category->save();
        return redirect('/category')->with([
            'flash_message' => 'Category has been updated!',
            'flash_message_important' => true
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect('/articles')->with([
            'flash_message' => 'Category has been deleted!',
            'flash_message_important' => true
        ]);
    }
}
