<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CommentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if(Input::get('comment') == ''){

            $result['status'] = 0;
            $result['message'] = 'Comment tidak boleh kosong';
        }else{
            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->article_id = Input::get('article_id');
            $comment->comment = Input::get('comment');
            if($comment->save()){
                $result['status'] = 1;
                $result['id'] = $comment->id;
                $result['comment_name'] = Auth::user()->name;
            }else{
                $result['status'] = 0;
                $result['message'] = 'Kesalahan pada server';
            }
        }
        echo json_encode($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        if(!$this->isAuthorized($comment->user_id)){
            $result['status'] = 0;
            $result['message'] = 'Unauthorized';
        }
        else{
            $comment->delete();
            $result['status'] = 1;
        }
        echo json_encode($result);
    }

    private function isAuthorized($user_id)
    {
        return (Auth::user()->id == $user_id);
    }
}
