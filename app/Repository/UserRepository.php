<?php


namespace App\Repository;


use App\User;

class UserRepository {
    public function findByEmailOrCreate($userData, $provider)
    {
//        return User::firstOrCreate([
//            'email' => $userData->email,
//            'name' => $userData->name,
//            'provider_id' => $userData->id,
//            'provider' => $provider
//        ]);
        $user = User::firstOrNew([
            'email' => $userData->email
        ]);
        if($user->name == '') //new user via social login
        {
            $user->name = $userData->name;
            $user->provider_id = $userData->id;
            $user->provider = $provider;
            $user->save();
        }
        elseif($user->provider_id == '') //pernah register via email, tp skrg mau via facebook
        {
            $user->provider_id = $userData->id;
            $user->provider = $provider;
            $user->save();
        }
        $this->checkIfUserNeedsUpdating($userData, $user);
        return $user;

    }

    public function checkIfUserNeedsUpdating($userData, $user) {

        $socialData = [
            'email' => $userData->email,
            'name' => $userData->name,
        ];
        $dbData = [
            'email' => $user->email,
            'name' => $user->name,
        ];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->email = $userData->email;
            $user->name = $userData->name;
            $user->save();
        }
    }
}