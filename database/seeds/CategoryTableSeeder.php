<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert(array(
            array(
            'name'     => 'Life',
            'slug'    => 'life',
            ),
            array(
                'name'     => 'Inspiration',
                'slug'    => 'inspiration',
            ),
            array(
                'name'     => 'Quote',
                'slug'    => 'quote',
            ),
        ));
    }
}
