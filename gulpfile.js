var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'app.css',
        'libs/bootstrap.min.css',
        'libs/select2.min.css'
    ]);

    mix.scripts([
        'libs/jquery-1.11.3.min.js',
        'libs/bootstrap.min.js',
        'libs/modernizr-2.8.3.min.js',
        'libs/select2.min.js'
    ]);

    mix.version(['public/css/all.css', 'public/js/all.js']);
});
