@extends('layout.master')

@section('content')
    <h1>Articles</h1>
    {!! Form::model($category = new \App\Category,['url' => 'category']) !!}
    @include('partials.category-form',['submitButton' => 'Add Category'])
    {!! Form::close() !!}
    @include('errors.list')
@stop