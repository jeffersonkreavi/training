@extends('layout.master')

@section('content')
    <h1>Articles</h1>
    {!! Form::model($category, ['method' => 'PATCH', 'url' => 'category/'.$category->id]) !!}
    @include('partials.category-form',['submitButton' => 'Update Category'])
    {!! Form::close() !!}
    @include('errors.list')
@stop