@extends('layout.master')

@section('content')
    <div class="row">
        <h1>Category</h1>
        <a href="category/create"><button class="btn btn-default">CREATE</button></a>
    </div>
    <table class="table .table-hover">
        <thead>
            <tr>
                <th>Category Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
                <tr>
                    <td class="col-md-3"><a href="{{url('/category', $category->slug)}}">{{ $category->name }}</a></td>
                    <td><a href="/category/{{$category->id}}/edit"><button class="btn btn-primary">Edit</button></a></td>
                </tr>

                    {{--<a href=""><button class="btn btn-danger">Delete</button></a>--}}
            @endforeach
        </tbody>
    </table>
    {!! $categories->render() !!}
@stop