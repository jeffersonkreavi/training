<div class="form-group">
    {!! Form::label('name', 'Category Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Category Name']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitButton, ['class' => 'form-control btn btn-primary']) !!}
</div>