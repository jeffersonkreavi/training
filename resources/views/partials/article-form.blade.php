<div class="form-group">
	{!! Form::label('title', 'Title:') !!}
	{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
</div>
<div class="form-group">
	{!! Form::label('content', 'Content:') !!}
	{!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('short_description', 'Short Description:') !!}
	{!! Form::textarea('short_description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Image') !!}
    @if($article->image)
        <img src="/images/original/{{$article->image.".".$article->imgtype}}">
    @endif
    {!! Form::file('image', null) !!}
</div>
<div class="form-group">
    {!! Form::label('category_list', 'Category:') !!}
    {!! Form::select('category_list', $categories, $article->category_id, ['id' => 'tag_list', 'class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::submit($submitButton, ['class' => 'form-control btn btn-primary']) !!}
</div>