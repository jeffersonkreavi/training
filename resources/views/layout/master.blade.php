{{-- Stored in resources/views/layouts/master.blade.php --}}
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{elixir('css/all.css')}}">
    {{--<link rel="stylesheet" href="/css/all.css">--}}

</head>
<body>
@include('layout.header')
<div class="container">
    @include('partials.flash')
    @yield('content')
</div>
<script src="{{elixir('js/all.js')}}"></script>
@if(isset($js))
    @foreach($js as $script)
        <script src="/js/{{ $script }}"></script>
    @endforeach
@endif
<script>
    $(document).ready(function(){
        $('#alert').not('.alert-important').delay(3000).slideUp(300);
    });
</script>
@yield('footer')
</body>
</html>