@extends('layout.master')

@section('content')
    <div class="row">
        <h1>Articles</h1>
        <a href="/articles/create"><button class="btn btn-default">CREATE</button></a>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="col-md-6">Title</th>
                <th colspan="2" style="text-align: center">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td class="col-md-6">
                    <a href="{{url('/articles', $article->slug)}}">{{ $article->title }}</a>
                </td>
                <td>
                    <a href="/articles/{{ $article->id }}/edit"><button class="btn btn-primary">Edit</button></a>
                </td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'url'=>'/articles/'.$article->id]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm(\'Are you sure?\')']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $articles->render() !!}
@stop