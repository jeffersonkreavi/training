@extends('layout.master')

@section('content')
	<h1>{{ $article->title}}</h1>
    <img src="/images/original/{{  $article->image.".".$article->imgtype  }}">
	<article>
        {{ $article->title }}
    </article>
    <p>{{$article->content}}</p>
    <div id="comment-section">
        <div class="row"><h3>COMMENTS</h3></div>
        @if($user)
            <div class="form-group">
                {!! Form::open(['url' => 'comment/store', 'id' => 'comment-form']) !!}
                    {!! Form::hidden('article_id', $article->id) !!}
                    {!! Form::text('comment', null, ['class' => 'form-control', 'placeholder' => 'Insert Comment']) !!}
                    {!! Form::submit('Add Comment', ['class' => 'form-control btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        @else
            <h4>You have to <a href="/auth/login">logged in</a> to comment.</h4>
        @endif
        <div id="comment-list">
            @foreach($article->comments as $comment)
                <div class="row comment">
                    <div class="col-md-2 comment-name">
                        {{$comment->user->name}}
                    </div>
                    <div class="col-md-6 comment-content">
                        {{$comment->comment}}
                    </div>
                    @if($user && $user->id == $comment->user_id)
                        <div class="col-md-1">
                            <button class="btn btn-danger delete-comment" data-id="{{$comment->id}}">&times;</button></a>
                        </div>
                    @endif

                </div>
            @endforeach
        </div>
    </div>

@stop