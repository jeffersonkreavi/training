@extends('layout.master')

@section('content')
	<h1>Articles</h1>
	@foreach($articles as $article)
		<article>
            <img src="/images/thumbnail/{{$article->image.".jpg"}}">
			<h2><a href="{{url('/articles', $article->slug)}}">{{ $article->title }}</a></h2>
            <p><a href="/category/{{ $article->category->slug }}">{{ $article->category->name }}</a></p>
			<div class="body">{{ $article->short_description }}</div>
		</article>
	@endforeach
    {!! $articles->render() !!}
@stop