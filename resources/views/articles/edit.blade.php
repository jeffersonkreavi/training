@extends('layout.master')

@section('content')
 <h1>Edit: {!! $article->title !!}</h1>
 {!! Form::model($article, ['method' => 'PATCH', 'url' => 'articles/'.$article->id, 'files' => true]) !!}
    @include('partials.article-form',['submitButton' => 'Update Article'])
 {!! Form::close() !!}
 @include('errors.list')

@stop