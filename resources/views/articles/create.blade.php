@extends('layout.master')

@section('content')
	<h1>Articles</h1>
	{!! Form::model($article = new \App\Article,['url' => 'articles', 'files' => true]) !!}
		@include('partials.article-form',['submitButton' => 'Add Article'])
	{!! Form::close() !!}
	@include('errors.list')
@stop