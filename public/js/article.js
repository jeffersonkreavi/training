$(document).ready(function(){
    $('#comment-form').on('submit', function(){
        $("input[type='submit']").prop('disabled', true);
        var token = $("input[name='_token']").val();
        var comment = $("input[name='comment']").val();
        if(comment.trim() == ''){
            alert("Silahkan masukkan comment");
            $("input[name='comment']").val('');
            $("input[type='submit']").prop('disabled', false);
            return false;
        }
        var article_id = $("input[name='article_id']").val();
        $.ajax({type: "POST", url: "/comment", data:{_token: token, article_id: article_id,
            comment: comment}}).done(function($result){
            var json = JSON.parse($result);
            if(json.status == 1){
                var newCommentDOM = "<div class='row comment'>" +
                "<div class='col-md-2 comment-name'>" + json.comment_name +
                "</div><div class='col-md-6 comment-content'>" + comment +
                    "</div><div class='col-md-1'>" +
                    "<button class='btn btn-danger delete-comment' data-id='" + json.id +
                    "'>&times;</button></a>"
                    "</div></div>";
                $("#comment-list").prepend(newCommentDOM);
                $("input[name='comment']").val('');
                $("input[type='submit']").prop('disabled', false);
            }else{
                $("input[type='submit']").prop('disabled', false);
                alert(json.message);
            }
        });
        return false;
    });

    $("#comment-list").on('click', '.delete-comment', function(){
        var $parent = $(this).parent().parent();
        var id = $(this).data('id');
        $.ajax({type: "DELETE", url: "/comment/"+id}).done(function($result){
            var json = JSON.parse($result);
            if(json.status == 1){
                $parent.remove();
            }else{
                alert(json.message);
            }

        });
    });
});